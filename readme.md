# Portfolio Charles Garoux

**Présentation** :
    Ce site a pour but d’aider les personnes intéressées par mon profil professionnel à mieux
me connaître. On y retrouve toutes les informations nécessaires à savoir sur mon profil :
* Ma présentation
* Mes technologies avec l’affinité et l’expérience
* Mes expériences et projets

**Le site est accessible a l'addresse** : [https://charles-garoux.dev](https://charles-garoux.dev/?o=gitlab&so=portfolio)

## Stack technique du projet

![Stack technique du projet](docs/schemas/tech-stack.png "Stack technique du projet")

### Hébergement sur Amazone Web Services

Les services utilisés :
* Stockage de l'application : **S3**
* CDN : **CloudFront**
* Gestion des certificats SSL : **Certificate Manager**

> Le déploiement des ressources Cloud a été fait manuellement.

## Autre

**Auteurs** :
* **Charles Garoux** (développeur)

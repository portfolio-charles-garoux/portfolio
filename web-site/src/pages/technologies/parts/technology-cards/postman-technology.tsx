import React from 'react';
import TechCard from '../../../../components/technologies-page/tech-card';
import {TechnologyCardType} from '../../../../data-structures/components/technology-card.type';
import InternshipMillionVictories from '../experience-links/internship-million-victories';

const PostmanTechnology: TechnologyCardType = ({cardSize = 'large'}) => {
    const technologyName = 'Postman';

    return (
        <TechCard className="border-warning"
                  imageSource="img/technologies/postman_logo.svg"
                  technologyName={technologyName}
                  imageWidthInPercent={40}
                  cardSize={cardSize}>

            <TechCard.Gain>
                Postman me permet de travailler facilement avec des API HTTP et de créer facilement des requêtes HTTP
                avancées.
            </TechCard.Gain>

            <TechCard.AddInfo technologyName={technologyName}>
                <TechCard.AddInfo.Affinity
                    technologyName={technologyName}
                    gradeValue={3}>
                    J'apprécie le confort apporté pour travailler avec des requêtes HTTP, mais je ne suis pas encore à
                    l’aise avec l’outil et ses capacités.
                </TechCard.AddInfo.Affinity>
                <TechCard.AddInfo.Experience
                    technologyName={technologyName}
                    gradeValue={1.5}>
                    Je nécessite de beaucoup plus d'expérience et des cas d'utilisation spéciaux pour connaître plus en
                    profondeur les différentes capacités de Postman, mais je l’utilise quand j’ai à manipuler des
                    requêtes HTTP.
                </TechCard.AddInfo.Experience>
            </TechCard.AddInfo>

            <TechCard.Experiences.Group>
                <TechCard.Experiences.Group.Element>
                    <TechCard.Experiences.Group.Element.Button.Link
                        technologyName={technologyName}
                        experienceLinkData={InternshipMillionVictories}/>
                </TechCard.Experiences.Group.Element>
            </TechCard.Experiences.Group>
        </TechCard>
    );
};

export default PostmanTechnology;
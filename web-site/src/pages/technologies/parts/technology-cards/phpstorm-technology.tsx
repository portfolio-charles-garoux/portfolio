import React from 'react';
import TechCard from '../../../../components/technologies-page/tech-card';
import {TechnologyCardType} from '../../../../data-structures/components/technology-card.type';

const PhpStormTechnology: TechnologyCardType = ({cardSize = 'large'}) => {
    const technologyName = 'PhpStorm';

    return (
        <TechCard className="border-warning"
                  imageSource="img/technologies/phpstorm_logo.svg"
                  technologyName={technologyName}
                  imageWidthInPercent={40}
                  cardSize={cardSize}
                  hidden>

            <TechCard.Gain>
                PhpStorm est l’IDE qui me permet de travailler confortablement sur différentes technologies que
                j’utilise.
            </TechCard.Gain>

            <TechCard.AddInfo technologyName={technologyName}>
                <TechCard.AddInfo.Affinity
                    technologyName={technologyName}
                    gradeValue={4}>
                    J'apprécie les outils intégrés qui me permettent de gagner en performance sur le développement de
                    projet, mais il manque parfois de plugin de qualité pour la compatibilité avec certaines
                    technologies.
                </TechCard.AddInfo.Affinity>
                <TechCard.AddInfo.Experience
                    technologyName={technologyName}
                    gradeValue={2.5}>
                    Je nécessite de plus d'expérience et des cas d'utilisation spéciaux pour connaître plus en
                    profondeur les différentes capacités de l’IDE, mais je l’utilise constamment dans mon travail.
                </TechCard.AddInfo.Experience>
            </TechCard.AddInfo>
        </TechCard>
    );
};

export default PhpStormTechnology;
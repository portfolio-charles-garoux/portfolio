import {ExperienceUdemyModalInterface} from '../../../../data-structures/components/experience-udemy-modal.interface';
import PracticalBeginnersCourseToServerlessApplicationsUdemy
    from './experience-udemy/praticle-beginner-course-to-serverless-applications-udemy';

const ServerlessFrameworkUdemyModal: ExperienceUdemyModalInterface = {
    name: 'Formation Udemy',
    udemyExperiences: [
        PracticalBeginnersCourseToServerlessApplicationsUdemy
    ]
};

export default ServerlessFrameworkUdemyModal;
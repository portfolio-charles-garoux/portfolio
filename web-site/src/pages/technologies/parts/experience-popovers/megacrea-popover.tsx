/**
 * Inutilisé depuis que la vidéo de présentation de stage a été publié
 */

import React, {FunctionComponent} from 'react';
import {ExperiencePopoverInterface} from '../../../../data-structures/components/experience-popover.interface';
import Alinea from '../../../../components/alinea';

const MegaCreaPopoverTitle: FunctionComponent = () => (
    <span>Stage à MégaCréa <span className="fw-light">(en cours)</span></span>
);

const MegaCreaPopoverDescription: FunctionComponent = () => (
    <p>
        <Alinea/>Stage en développement d’outils (Web et CLI) pour une entreprise de E-commerce. <br/>
        <Alinea/>Les outils optimisent le traitement de photos venant du fournisseur à destination du site de vente en
        ligne.
    </p>
);

const MegacreaPopover: ExperiencePopoverInterface = {
    name: 'Stage à MégaCréa',
    Title: MegaCreaPopoverTitle,
    Description: MegaCreaPopoverDescription,
    technologies: [
        'Node.js',
        'TypeScript',
        'Express.js',
        'EJS',
        'Oclif',
        'Docker',
        'Gitlab CI'
    ]
};

export default MegacreaPopover;
import {ExperienceLinkInterface} from '../../../../data-structures/components/experience-link.interface';

const InternshipMillionVictories: ExperienceLinkInterface = {
    name: 'Stage à Million Victories',
    link: 'https://youtu.be/XItV_9TzEac',
    isExternalLink: true,
    type: 'youtube-video'
};

export default InternshipMillionVictories;
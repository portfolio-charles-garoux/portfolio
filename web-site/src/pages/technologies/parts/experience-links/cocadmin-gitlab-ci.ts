import {ExperienceLinkInterface} from '../../../../data-structures/components/experience-link.interface';

const CocadminGitlabCI: ExperienceLinkInterface = {
    name: 'Formation "Tout Automatiser avec GitLab"',
    link: 'https://cours.cocadmin.com/tout-automatiser-avec-gitlab',
    isExternalLink: true,
    type: 'web-site'
};

export default CocadminGitlabCI;
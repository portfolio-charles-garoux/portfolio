import {ExperienceLinkInterface} from '../../../../data-structures/components/experience-link.interface';

const CocadminDocker: ExperienceLinkInterface = {
    name: 'Formation "Docker : Le couteau suisse du DevOps"',
    link: 'https://cours.cocadmin.com/docker-le-couteau-suisse-du-devops',
    isExternalLink: true,
    type: 'web-site'
};

export default CocadminDocker;
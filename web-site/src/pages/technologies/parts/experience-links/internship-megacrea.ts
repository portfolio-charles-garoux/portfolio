import {ExperienceLinkInterface} from '../../../../data-structures/components/experience-link.interface';

const InternshipMegaCrea: ExperienceLinkInterface = {
    name: 'Stage à MégaCréa',
    link: 'https://youtu.be/UEMWap3eono',
    isExternalLink: true,
    type: 'youtube-video'
};

export default InternshipMegaCrea;
import {ExperienceLinkInterface} from '../../../../data-structures/components/experience-link.interface';

const CocadminAnsible: ExperienceLinkInterface = {
    name: 'Formation "Maitriser Ansible"',
    link: 'https://cours.cocadmin.com/maitriser-ansible',
    isExternalLink: true,
    type: 'web-site'
};

export default CocadminAnsible;
import {FunctionComponent} from 'react';

/**
 * /!\ Attention : encore en Bootstrap 4
 */

const WebCV: FunctionComponent = () => {
    return (
        <div>
            <div className="row my-3">
                <div className="col-xl-9 offset-xl-1 col-lg-10 offset-lg-1 col-12">
                    <button type="button" className="btn btn-block btn-outline-secondary" data-toggle="collapse"
                            data-target="#CVWeb" aria-expanded="false" aria-controls="CVWeb">
                        CV Web
                        <img src="svg/chevron-down-circle-outline.svg"/>
                    </button>
                </div>
            </div>

            {/* Refaire la section CV Web avec tailwindcss (et avec timeline) */}
            <div className="collapse" id="CVWeb">
                <div className="row">
                    <div className="offset-lg-2 col-md-8 offset-md-1 col-sm-12">
                        <h1>Expériences professionnelles</h1>
                        <ul>
                            <li>2020 du 1 juin au 30 novembre - Stage de <a href="https://www.42lyon.fr/"
                                                                            target="blank">42 Lyon</a> au studio de jeu
                                vidéo
                                <a href="http://millionvictories.com/" target="_blank" rel="noopener noreferrer">Million Victories</a>
                                pour travailler sur <a href="https://youtu.be/XItV_9TzEac" target="_blank" rel="noopener noreferrer">différents
                                    aspects du développement Back-end et d’infrastructure Cloud</a>
                            </li>
                            <li>2018 du 22 mai au 22 juin - Stage de 1<sup>ère</sup> année en BTS SIO option SLAM dans
                                l'association
                                "303 airsoft team" pour développer l'application web 303Event
                            </li>
                            <li>2017 du 17 juillet au 30 août - <span className="hover-info">Employé à l'espace culturel<span
                                className="hover-info-text text-light bg-info">Caissier, préparateur de commande, mise en rayon jeux vidéo, conseiller en rayon jeux vidéo</span></span>
                                du <a href="http://www.e-leclerc.com/andrezieux" target="_blank" rel="noopener noreferrer">Leclerc
                                    d'Andrézieux-Bouthéon</a></li>
                            <li>2016 le 26 novembre - <a
                                title="Récupération de don et trie avant expédition à l'entrepôt">Bénévolat
                                avec la Banque alimentaire</a></li>
                            <li>2015 - Bénévolat comme serveur pour le repas jour de l'an organisé par la commune de
                                Saint-Jean-Bonnefonds
                            </li>
                            <li>2014 - Stage de 3<sup>ème</sup> à la Minoterie Dupuy Couturier une semaine</li>
                        </ul>
                    </div>
                </div>

                <div className="row">
                    <div className="offset-lg-2 col-md-8 offset-md-1 col-sm-12">
                        <h1>Formation</h1>
                        <ul>
                            <li>Octobre 2018 à aujourd'hui - En formation à l'école <a href="https://www.42lyon.fr/"
                                                                                       target="blank">42
                                Lyon</a>.
                            </li>
                            <li>Août 2018 - Piscine de sélection à l'école <a href="https://www.42lyon.fr/"
                                                                              target="blank">42
                                Lyon</a>, sélectionné.
                            </li>
                            <li>Octobre 2017 - Piscine de sélection à l'école <a href="https://www.42lyon.fr/"
                                                                                 target="blank">42
                                Lyon</a>, non sélectionné.
                            </li>
                            <li>Septembre 2017 à juin 2018 - 1<sup>ère</sup> année de BTS SIO en option SLAM au <a
                                href="http://simone-weil.elycee.rhonealpes.fr/" target="blank">lycée Simone Weil</a>
                            </li>
                            <li>Septembre 2016 à juin 2017 - <span className="hover-info">BAC STI2D SIN<span
                                className="hover-info-text text-light bg-info">Sciences et Technologies de l'Industrie et du Développement Durable Systèmes d'Information et Numérique</span></span>
                                au <a href="http://mauriac-desgranges.elycee.rhonealpes.fr" target="blank">lycée
                                    François-Mauriac</a></li>
                        </ul>
                    </div>
                </div>

                <div className="row">
                    <div className="offset-lg-2 col-md-8 offset-md-1 col-sm-12">
                        <h1>Compétences spécifiques</h1>
                        <h4>Informatique:</h4>
                        <ul>
                            <li>Connaissances essentielles de réseau informatique</li>
                            <li>Modélisation de données en schéma entité association</li>
                            <li>Traduire et convertir Décimal/Hexadécimal/Binaire</li>
                            <li>Manie la logique combinatoire (fonction logique, tableau de Karnaugh)</li>
                        </ul>
                        <h4>Programmation:</h4>
                        <h6 className="pl-2">
                            <b>Plus de détails sur la page <a href="technologies.html">Technologies</a></b>
                        </h6>
                        <ul>
                            <li>Développement Web (spécialisé Back-end)</li>
                            <li>Développement Systeme</li>
                        </ul>
                    </div>
                </div>

                <div className="row">
                    <div className="offset-lg-2 col-md-8 offset-md-1 col-sm-12">
                        <h1>Centres d’intérêt</h1>
                        <h4>Mes activités sportives:</h4>
                        <ul>
                            <li>Skateboard</li>
                            <li>Balade en vélo</li>
                            <li>Airsoft</li>
                        </ul>
                        <h4>Mes passions:</h4>
                        <ul>
                            <li>Jeux vidéo</li>
                            <ul>
                                <li>Jeux de stratégie</li>
                                <li>Simulations militaire</li>
                                <li>Divers jeux indépendants</li>
                            </ul>
                            <li>Jeu de rôle papier</li>
                            <li>Airsoft</li>
                            <li>La fabrication additive</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default WebCV;
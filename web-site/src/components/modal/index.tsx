import React, {FunctionComponent} from 'react';

interface ModalButtonProps {
    modalId: string
    className?: string
}

export const ModalButton: FunctionComponent<ModalButtonProps> = ({modalId, children}) => (
    <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target={`#${modalId}`}>
        {children}
    </button>
);

interface ModalHeaderProps {
    modalId: string
    modalTitle: string
    closeCross?: boolean
}

export const ModalHeader: FunctionComponent<ModalHeaderProps> = ({modalId, modalTitle, closeCross = true, children}) => (
    <div className="modal-header">
        <h5 className="modal-title" id={`${modalId}Label`}>{modalTitle}</h5>
        {closeCross &&
        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"/>
        }
    </div>
);

export const ModalBody: FunctionComponent = ({children}) => (
    <div className="modal-body">
        {children}
    </div>
);

export const ModalFooter: FunctionComponent = ({children}) => (
    <div className="modal-footer">
        {children}
    </div>
);

interface ModalProps {
    modalId: string
}

const Modal: FunctionComponent<ModalProps> = ({modalId, children}) => (
    <div className="modal fade" id={modalId} tabIndex={-1} aria-labelledby={`${modalId}Label`}
         aria-hidden="true">
        <div className="modal-dialog">
            <div className="modal-content">
                {children}
            </div>
        </div>
    </div>
);

export default Modal;
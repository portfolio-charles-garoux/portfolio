import React, {FunctionComponent} from 'react';
import TechnologyBaseGrade from './base-grade';
import {PopoverPlacementType} from '../../../../../data-structures/types/popoverPlacement.type';

type Props = {
    technologyName: string
    blockButton?: boolean
    popoverPlacement?: PopoverPlacementType
    colorTheme?: string
    gradeValue: number
    gradeMax?: number
    buttonSize?: 'sm' | 'lg'
}

/**
 * Bouton avec la note d'experience et en popover la raison (ou autre) de la note.
 * Le props "children" contiendra la raison
 */
const TechnologyExperienceGrade: FunctionComponent<Props> = (
    props) => {
    return (
        <TechnologyBaseGrade {...props} gradeSymbole="star" gradeType="experience" titlePrefix="Expérience avec"/>
    );
};


export default TechnologyExperienceGrade;
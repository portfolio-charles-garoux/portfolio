
export function sanitiseNameForId(str: string) {
    return str.replace(/[\s:()]/g, '-').toLowerCase()
}
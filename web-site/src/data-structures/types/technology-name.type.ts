export type TechnologyName =
    'HTML' |
    'CSS' |
    'JavaScript' |
    'TypeScript' |
    'Bootstrap' |
    'React' |
    'Nest.js' |

    'PHP' |
    'Laravel' |
    'Lumen' |

    'Node.js' |
    'Typescript' |
    'Express.js' |
    'EJS' |
    'Oclif' |

    'Serverless' |

    'MariaDB' |
    'SQL' |
    'MongoDB' |

    'Bash' |
    'Ansible' |

    'AWS' |
    'GCP' |
    'Scaleway' |

    'Terraform' |
    'Kubernetes' |

    'Docker' |
    'Docker Compose' |

    'Apache2' |

    'Gitlab CI' |
    'Azure DevOps' |
    'Dagger.io' |

    'Elasticsearch' |
    'Logstash' |
    'Kibana' |

    'Unity'
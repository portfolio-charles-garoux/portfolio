type ExperienceType = 'project' | 'internship' | 'formation';

interface Experience {
    name: string
    type: ExperienceType
    shortDescription : string
    description : string
    publicLink: string[]
}

interface ExperienceWith {
    project : Experience
    technologie : Technology[]
}

export default interface Technology {
    name : string
    shortDescription : string
}


// Tech - 0 -- - Experience
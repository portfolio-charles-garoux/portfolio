import {FunctionComponent} from 'react';
import {TechnologyName} from '../types/technology-name.type';

export interface ExperienceModalInterface {
    name: string
    Title: FunctionComponent<{}>
    Context: FunctionComponent<{}>
    Goal: FunctionComponent<{}>
    Result: FunctionComponent<{}>
    info: {
        learningNote: number
        enjoyingNote: number
    }
    git?: {
        public: boolean
        platform? : 'Gitlab' | 'Github'
        link?: string
    }
    technologiesUsed ?: TechnologyName[]
}
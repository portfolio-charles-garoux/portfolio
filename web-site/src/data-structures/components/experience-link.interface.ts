export interface ExperienceLinkInterface {
    name: string
    link: string
    isExternalLink: boolean
    type?: 'youtube-video' | 'web-site'
}
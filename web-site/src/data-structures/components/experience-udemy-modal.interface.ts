import {FunctionComponent} from 'react';

export interface ExperienceUdemyInterface {
    name: string
    certificatLink: string
    Presentation: FunctionComponent<{}>
}

export interface ExperienceUdemyModalInterface {
    name: string
    udemyExperiences: ExperienceUdemyInterface[]
}

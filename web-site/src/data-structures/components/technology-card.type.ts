import {FunctionComponent} from 'react';
import {CardColSize} from '../../components/technologies-page/tech-card';

type TechnologyCardProps = {
    cardSize?: CardColSize
}

export type TechnologyCardType = FunctionComponent<TechnologyCardProps>;
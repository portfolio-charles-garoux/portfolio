import {FunctionComponent} from 'react';
import {TechnologyName} from '../types/technology-name.type';

export interface ExperiencePopoverInterface {
    name: string
    Title: FunctionComponent<{}>
    Description: FunctionComponent<{}>
    technologies: TechnologyName[]
}
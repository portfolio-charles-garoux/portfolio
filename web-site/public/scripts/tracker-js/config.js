let Config = {
    postTrackLog: {
        dev: "https://8cwmvz9fff.execute-api.us-east-1.amazonaws.com/dev/track",
        prod: "https://vrte74ifv3.execute-api.eu-west-3.amazonaws.com/prod/track"
    }
};

export {Config};

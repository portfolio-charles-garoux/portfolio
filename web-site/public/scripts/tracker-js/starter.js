// console.log("Star tracking");
import {TrackEngine} from "./Library/TrackEngine.js";
import {TimeTool} from "./Library/TimeTool.js";
import {parseTrackDataFromHtml} from "./Library/parsing.js";
import LeavingHandler from "./Library/leaving-handler.js";

TrackEngine.setVisitorIP()
    .then((visitorIP) => {
        const timestamp = TimeTool.getTimestamp();
        const url = new URL(window.location.href);
        const origin = TrackEngine.getOrigin(url.searchParams.get("o"));
        const subOrigin = TrackEngine.getSubOrigin(url.searchParams.get("so"));

        let visitData = {
            visitorIP: visitorIP,
            page: window.location.href,
            actionType: "visit",
            timestamp: timestamp,
            timeForHuman: TimeTool.convertTimestampForHuman(timestamp),
            date: TimeTool.convertTimestampToDate(timestamp)
        };
        if (origin) {
            visitData.specialData = {
                origin: origin,
                subOrigin: subOrigin
            };
        }
        TrackEngine.pushTrackingData(JSON.stringify(visitData));

        setTrack();
        let lastUrl = document.URL;
        setInterval(() => {
            // Note au lecteur : c'est pas beau, comme le reste du script, ca sera remplace par du full React
            if (lastUrl !== document.URL) {
                lastUrl = document.URL;
                setTrack();
                // console.log('url change');
            }
        }, 500);
    })
    .catch((error) => {
        console.error(error);
    });


function setTrack() {
    let alreadyTrackedList = [];

    /**
     * Track hover on button (like popover)
     */
    let hoverButtonList = Array.from(document.querySelectorAll("[data-track-trigger='hover']"))
        .filter((element) => !alreadyTrackedList.includes(element));
    alreadyTrackedList = alreadyTrackedList.concat(hoverButtonList);

    TrackEngine.loadTracker(hoverButtonList, "mouseover", (event) => {
        const secondsBeforeRepeat = 10;
        const target = event.target.closest('.btn');
        const timestamp = Date.now();
        const lastHover = parseInt(target.dataset.trackLiveLastTrigger, 10);

        if (isNaN(lastHover) || target.dataset.trackLiveLastTrigger < timestamp - (1000 * secondsBeforeRepeat)) // First trigger || Re-trigger after "secondsBeforeRepeat" seconds
            target.dataset.trackLiveLastTrigger = timestamp.toString();
        else
            return;

        const trackData = parseTrackDataFromHtml(target);

        const specialData = Object.assign({
            targetType: "hover"
        }, trackData);

        const dataJSON = TrackEngine.formatData(event, target, specialData);

        TrackEngine.pushTrackingData(dataJSON);
    });

    /**
     * Track click on button
     */
    let buttonList = Array.from(document.getElementsByClassName("btn"))
        .filter((element) => !alreadyTrackedList.includes(element));
    alreadyTrackedList = alreadyTrackedList.concat(buttonList);

    TrackEngine.loadTracker(buttonList, "click", (event) => {
        const target = event.target.closest('.btn');

        const trackData = parseTrackDataFromHtml(target);

        const specialData = Object.assign({
            targetType: "button"
        }, trackData);

        const dataJSON = TrackEngine.formatData(event, target, specialData);

        TrackEngine.pushTrackingData(dataJSON);
    });

    /**
     * Track link (<a>)
     */
    let linkList = Array.from(document.querySelectorAll("a"))
        .filter((element) => !alreadyTrackedList.includes(element));
    alreadyTrackedList = alreadyTrackedList.concat(linkList);

    TrackEngine.loadTracker(linkList, "click", (event) => {
        const target = event.target;
        // console.log(event);
        let text = TrackEngine
            .findAttributInPath(event.path, 'title', 'innerHTML')
            .replace(/<\/?[^>]+(>|$)/g, ""); // Delete HTML tag (not bullet proof), source : https://stackoverflow.com/questions/5002111/how-to-strip-html-tags-from-string-in-javascript

        const dataJSON = TrackEngine.formatData(event, target, {
            targetType: "link",
            href: TrackEngine.findAttributInPath(event.path, 'href'),
            text: text
        });
        TrackEngine.pushTrackingData(dataJSON);
    });

    LeavingHandler.init();
}

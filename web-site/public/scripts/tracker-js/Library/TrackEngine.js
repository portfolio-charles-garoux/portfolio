import {TimeTool} from "./TimeTool.js";
import {Config} from "../config.js";

const env = (
    window.location.hostname.includes('localhost') ||
    window.location.hostname.includes('aws') ||
    window.location.hostname.includes('s3') ||
    window.location.hostname.includes('172.')
) ?
    'dev' :
    'prod';
console.log('env', env)

let TrackEngine = {

    /**
     * visitorIP need to be set at start
     */
    visitorIP : null,
    pushTarget : Config.postTrackLog[env],

    /**
     *  Load reaction (tracking action) to action (event) to a list of elements (Array)
     *
     * @param elementList Array
     * @param actionToTrack String
     * @param reaction Function(event)
     */
    loadTracker : function (elementList, actionToTrack, reaction) {
        // console.log("loadTracker", elementList, reaction);//debug
        // console.log(typeof elementList,typeof elementList[0],elementList[0])

        elementList.forEach(element => {
            element.addEventListener(actionToTrack, reaction);
        });
    },

    /**
     * @param data : JSON
     */
    pushTrackingData: function (data) {
        return fetch(this.pushTarget, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        })
            .then(response => response.json())
            .then((responseData => {
                // console.log(responseData);
            }));
    },

    /**
     *
     * @return JSON
     */
    formatData : function (event, target, specialData) {
        const timestamp = TimeTool.getTimestamp();
        const data = {
            visitorIP : this.visitorIP,
            page : target.baseURI,
            actionType: event.type,
            timestamp : timestamp,
            timeForHuman : TimeTool.convertTimestampForHuman(timestamp),
            date : TimeTool.convertTimestampToDate(timestamp),
            specialData: specialData
        };

        const dataJSON = JSON.stringify(data);
        return (dataJSON);
    },

    /**
     * Find the first attribut in the path
     * @param eventPath : event.path
     * @param attributSearched : string
     *  Ex : 'href'
     *  Dataset not supported !
     * @param attributSecondarySearched
     */
    findAttributInPath : function(eventPath, attributSearched, attributSecondarySearched = null) {
        for (const element of eventPath) {
            if (element[attributSearched])
                return (element[attributSearched]);
            else if (attributSecondarySearched && element[attributSecondarySearched])
                return (element[attributSecondarySearched]);
        }

        console.error("Attribute \""+ attributSearched +"\" not found in path");
        return undefined;
    },

    _simpleAjax : function(address, method) {

        const promise = new Promise(((resolve, reject) => {
            var xhr = new XMLHttpRequest();
            xhr.open(method, address);
            xhr.onload = function() {
                resolve(xhr);
            };
            xhr.send();
        }));

        return (promise);
    },

    setVisitorIP : async function () {
        const ajaxResponse = await this._simpleAjax('https://api.ipify.org/?format=json' , 'GET');

        if (ajaxResponse.status !== 200) {
            console.error(ajaxResponse);
            this.visitorIP = "error";
            return ;
        }

        return this.visitorIP = JSON.parse(ajaxResponse.responseText).ip;
    },

    getOrigin : function (originValue) {
        const originShortCut = {
            "in" : "linkedin",
            "sf" : "scanfactor",
        };

        return (originShortCut[originValue])? originShortCut[originValue]: originValue;
    },

    getSubOrigin : function (subOriginValue) {
        const subOriginShortCut = {
            "ps" : "post",
            "pr" : "profile",
        };

        return (subOriginShortCut[subOriginValue])? subOriginShortCut[subOriginValue]: subOriginValue;
    }
};

export { TrackEngine };

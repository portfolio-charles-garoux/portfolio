import {TrackEngine} from "./TrackEngine.js";

function addEvent(obj, evt, fn) {
    if (obj.addEventListener) {
        obj.addEventListener(evt, fn, false);
    } else if (obj.attachEvent) {
        obj.attachEvent("on" + evt, fn);
    }
}

const pathToNextPage = {
    '/': '/technologies',
    '/technologies': '/',
    '/evenements': '/'
};

const LeavingHandler = {
    visitorHasVisit: false,
    visitorIsInformed: false,

    init: () => {
        const startHistoryLength = localStorage.getItem('startHistoryLength');

        if (localStorage.getItem('visitorIsInformed') === 'true' ||
            parseInt(startHistoryLength, 10) < history.length) {
            console.log('Abort leaving alert');
            return;
        }

        if (startHistoryLength === null)
            localStorage.setItem('startHistoryLength', history.length.toString());

        addEvent(document, "mouseout", (e) => {
            e = e ? e : window.event;
            const from = e.relatedTarget || e.toElement;

            if ((!from || from.nodeName == "HTML")
                && localStorage.getItem('visitorIsInformed') !== 'true' &&
                parseInt(localStorage.getItem('startHistoryLength'), 10) === history.length
            ) {
                TrackEngine.pushTrackingData(TrackEngine.formatData(
                    {type: 'show confirm'},
                    {baseURI: window.location.href},
                    {
                        targetType: "leaving alert"
                    }));

                const acceptToMove = confirm("Vous n'avez pas visité d'autres pages du site.\n\n" +
                    "Souhaitez-vous visiter une autre page ?");
                localStorage.setItem('visitorIsInformed', 'true');

                TrackEngine.pushTrackingData(TrackEngine.formatData(
                    {type: 'confirm'},
                    {baseURI: window.location.href},
                    {
                        targetType: "leaving alert",
                        acceptToMove: acceptToMove
                    }))
                    .then(() => {
                        if (acceptToMove) // If "OK" selected : change page
                            window.location.pathname = pathToNextPage[window.location.pathname];
                    });


            }
        });
    }

};

export default LeavingHandler;
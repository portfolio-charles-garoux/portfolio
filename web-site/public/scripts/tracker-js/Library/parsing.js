function toCamelCase(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
        return index === 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
} // function source : https://stackoverflow.com/questions/2970525/converting-any-string-into-camel-case

export function parseTrackDataFromHtml(htmlElement) {
    const trackData = {};
    for (const dataKey in htmlElement.dataset) {
        const newKey = toCamelCase(dataKey.substring('track'.length));
        trackData[newKey] = htmlElement.dataset[dataKey];
    }

    return trackData;
}
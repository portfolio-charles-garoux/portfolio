let TimeTool = {

    getTimestamp : function () {
        return (Math.round(Date.now() / 1000));
    },

    convertTimestampForHuman : function (UNIX_timestamp){
        const a = new Date(UNIX_timestamp * 1000);
        let hour = a.getHours();
        hour = (hour >= 10)? hour: "0" + hour;
        let min = a.getMinutes();
        min = (min >= 10)? min: "0" + min;
        let sec = a.getSeconds();
        sec = (sec >= 10)? sec: "0" + sec;
        return this.convertTimestampToDate(UNIX_timestamp)+ ' ' + hour + ':' + min + ':' + sec
    },

    convertTimestampToDate : function (UNIX_timestamp){
        const a = new Date(UNIX_timestamp * 1000);
        const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        const year = a.getFullYear();
        const month = months[a.getMonth()];
        const date = a.getDate();
        return date + ' ' + month + ' ' + year;
    }

};

export { TimeTool };
